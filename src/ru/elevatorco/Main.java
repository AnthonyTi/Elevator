package ru.elevatorco;

import ru.elevatorco.models.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int f = 6;
        double h = 1.0;
        double s = 1.0;
        double t = 1.0;

        for (int i = 0; i < args.length; i++) {
            //-F 10 -H 2.5 -S 1.5 -T 1.5
            if (args[i].toLowerCase().equals("-f")) {
                try {
                    f = Integer.parseInt( args[i+1] );
                }catch( Exception e ) {
                    System.out.println(e.getMessage());
                } finally {
                    i++;
                }
            }
            if (args[i].toLowerCase().equals("-h")) {
                try {
                    h = Double.parseDouble( args[i+1] );
                }catch( Exception e ) {
                    System.out.println(e.getMessage());
                } finally {
                    i++;
                }
            }
            if (args[i].toLowerCase().equals("-s")) {
                try {
                    s = Double.parseDouble( args[i+1] );
                }catch( Exception e ) {
                    System.out.println(e.getMessage());
                } finally {
                    i++;
                }
            }
            if (args[i].toLowerCase().equals("-t")) {
                try {
                    t = Double.parseDouble(args[i + 1]);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                } finally {
                    i++;
                }
            }
        }

        Building building = new Building(f, h);
        Elevator elevator = new Elevator(s, t, building);

        Passenger passenger = new Passenger(elevator, building);
    }
}
