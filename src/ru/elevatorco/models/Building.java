package ru.elevatorco.models;

public class Building {

    private int floor;
    private double height;

    public Building(int f, double h) {
        if (f < 5) {
            floor = 5;
        } else if (f > 20) {
            floor = 20;
        } else {
            floor = f;
        }
        height = h;
    }

    public double getHeight() {
        return height;
    }

    public int getFloor() {
        return floor;
    }
}
