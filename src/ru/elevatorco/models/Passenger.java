package ru.elevatorco.models;

import java.util.Scanner;

public class Passenger implements Runnable {

    private Position position;
    private Elevator elevator;
    private Building building;
    public Thread t;

    enum Position {
        INSIDE,
        OUTSIDE
    }

    public Passenger(Position p, Elevator elevator) {
        position = p;
        this.elevator = elevator;
    }

    public Passenger(Elevator elevator, Building building) {
        t = new Thread(this);
        position = Position.OUTSIDE;
        this.elevator = elevator;
        this.building = building;
        t.start();
    }

    private String passengerStatus() {
        String s = "";
        if (position == Position.INSIDE) {
            s = "Сейчас Вы находитесь в лифте. На какой этаж поедем? ";
        } else {
            s = "Сейчас Вы находитесь в подъезде. На какой этаж вызвать лифт? ";
        }
        return s;
    }

    @Override
    public void run() {
        try {
            System.out.println(elevator.getStatus());
            int n = 1;
            Scanner sc = new Scanner(System.in);
            while (true) {
                System.out.print(passengerStatus());
                if (sc.hasNextInt()) {
                    n = sc.nextInt();
                    if (n < 1 || n > building.getFloor()) {
                        System.out.println("Вы можете ввести число от 1 до " + building.getFloor());
                        continue;
                    }
                }
                if (position == Position.OUTSIDE) {
                    elevator.call(n);
                    position = Position.INSIDE;
                } else if (position == Position.INSIDE) {
                    elevator.movingTo(n);
                    position = Position.OUTSIDE;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
