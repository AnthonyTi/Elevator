package ru.elevatorco.models.var;

public enum Direction {
    UP,
    DOWN
}