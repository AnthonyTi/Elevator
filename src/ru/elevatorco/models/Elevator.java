package ru.elevatorco.models;

import ru.elevatorco.models.var.*;

public class Elevator {

    private double speed;
    private double time;
    private int position;
    private State state = State.STOPPED;
    private Direction direction = Direction.UP;
    private Building building;
    private Door door = Door.CLOSED;


    public Elevator(double s, double t, Building b) {
        speed = s;
        time = t;
        position = 1;
        building = b;
    }

    void close() {
        try {
            door = Door.CLOSED;
            Thread.sleep((long) (time * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Лифт закрыл двери");
    }

    void open() {
        try {
            door = Door.OPEN;
            Thread.sleep((long) (time * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Лифт открыл двери");
    }

    void movingTo(int floorNumber) {
        if (door == Door.OPEN) {
            close();
        }
        direction = position > floorNumber ? Direction.DOWN : Direction.UP;
        state = State.MOVING;
        int gap = Math.abs(position - floorNumber);
        for (int i = 1; i <= gap; i++) {
            try {
                double delay = building.getHeight() / speed;
                Thread.sleep((long) (1000 * delay));
                position = position + (direction == Direction.UP ? 1 : -1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (position == floorNumber) {
                state = State.STOPPED;
                open();
            }
            System.out.println(getStatus());
        }
    }

    void call(int floor) {
        if (floor != position) {
            movingTo(floor);
        }
        if (door == Door.CLOSED) {
            open();
        }
    }

    public String getStatus() {
        String s = "";
        if (state == State.MOVING) {
            s = "Лифт проезжает " + position + " этаж" ;
        } else {
            s = "Лифт находится на " + position + " этаже. Двери " + (door == Door.CLOSED ? "закрыты" : "открыты") + ".";
        }
        return s;
    }
}
